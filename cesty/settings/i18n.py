# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

import os
from .common import BASE_DIR


LANGUAGE_CODE = 'en'

TIME_ZONE = 'Europe/Prague'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', 'English'),
    ('cz', 'Czech'),
)

LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale/'),)

DATETIME_INPUT_FORMATS = (
    '%Y-%m-%dT%H:%M', '%Y-%m-%dT%H:%M'
)

