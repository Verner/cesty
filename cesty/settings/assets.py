# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

import os

DATA_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))




#STATICFILES_DIRS = (
  #os.path.join(DATA_DIR, 'bower_components'),
  #os.path.join(DATA_DIR, 'static'),
#)

STATICFILES_FINDERS = (
    'assets.finders.ManifestFinder',
    'assets.finders.ManifestAppDirsFinder',
)

from assets.filters import register_filter
from assets.filters.utils import pipe

@register_filter('postcss')
def postcss_filter(bytes, cwd):
    return pipe('postcss --use autoprefixer', bytes, cwd=cwd)
