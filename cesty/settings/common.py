"""
Django settings for logika project.

"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


ALLOWED_HOSTS = []


# Application definition

ROOT_URLCONF = 'cesty.urls'

from .i18n import *
from .assets import *
from .apps import *
from .secrets import *
from .templates import *

SITE_ID = 1


MIGRATION_MODULES = {

}

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

FIXTURE_DIRS = (
 os.path.join(BASE_DIR, 'fixtures'),
)

SESSION_ENGINE='django.contrib.sessions.backends.cached_db'

LOGIN_URL='login'
