import os
from .common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cesty_dev',
        'USER': '',
        'HOST': '',
        'PORT': '',
    }
}

STATIC_URL = 'http://localhost:7132/static/'
MEDIA_URL = 'http://localhost:7132/media/'
CDN_ROOT = os.path.join(DATA_DIR, 'cdn')
STATIC_ROOT = os.path.join(CDN_ROOT, 'static')
MEDIA_ROOT = os.path.join(CDN_ROOT, 'media')

ENABLE_PROFILING_MIDDLEWARE=False
ENABLE_PROFILING_DECORATOR=False
PROFILE_BASE_DIR=os.path.join(BASE_DIR, "../profile/")

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

#CACHES = {
    #'default': {
        #'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        #'LOCATION': 'unique-snowflake',
    #}
#}
TEMPLATES[0]['OPTIONS']["auto_reload"]=True

