import logging
import os
import pathlib
import random
import sys

from io import StringIO

from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from django.conf import settings

from plumbum import SshMachine
from plumbum import local
from plumbum import NOHUP

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Deploy to production'
    SOURCE_DIRS = ['core/static/core/fonts']

    def handle(self, *args, **options):
        fontforge = local['fontforge']
        for sd in self.SOURCE_DIRS:
            path = pathlib.Path(sd)
            with local.cwd(path.absolute()):
                for item in pathlib.Path('.').iterdir():
                    if item.is_file() and item.name.endswith('.ttf'):
                        fontforge('-lang=ff', '-c', 'Open($1); Generate($2);', item.name,  item.name.replace('ttf','woff2'))
