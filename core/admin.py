from django.contrib import admin

# Register your models here.
from . import models

models = [
    models.Employee,
    models.Department,
    models.Journey,
    models.Account,
    models.AccountExpense,
    models.Expense,
    models.Document,
    models.Currency,
    models.Country,
]

for m in models:
    admin.site.register(m)



