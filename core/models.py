


# Create your models here.







class Approval(models.Model):

    user = orm.Required(User)
    type = orm.Required(str)
    signature = models.CharField(max_length=512, blank=True, null=True)


class JourneyApproval(Approval):
    journey = orm.Required(Journey)


class ApprovalRequest(models.Model):

    targets = models.ManyToManyField(User, reverse='target_requests')
    type = models.CharField(max_length=250,blank=True, null=True)
    pending = orm.Optional(bool)
    source = orm.Required(User, reverse='source_requests')





class ExpenseApproval(Approval):
    expenses = models.ManyToManyField(Expense)
