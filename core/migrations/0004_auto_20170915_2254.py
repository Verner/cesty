# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-15 20:54
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20170915_2233'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='country',
            name='perdiem',
        ),
        migrations.AddField(
            model_name='country',
            name='perdiems',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict),
        ),
    ]
