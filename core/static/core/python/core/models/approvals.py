from django.db import models

from .org import Employee
from .accounts import Expense
from .journeys import Journey

class Approval(models.Model):
    STATUS = (
        ('1-Approved', 'Schváleno'),
        ('2-NotApproved', 'Neschváleno'),
        ('3-NeedsChanges', 'Je třeba doplnit'))
    employee = models.ForeignKey(Employee)
    signature = models.CharField(max_length=512, blank=True, null=True)
    status = models.CharField(choices=STATUS, max_length=50)
    reason = models.TextField(blank=True, null=True)
    
class ApprovalRequest(models.Model):
    requested_by = models.ForeignKey(Employee, related_name='requests_by_me')
    request_for = models.ForeignKey(Employee, related_name='requests_on_me')

class ExpenseApproval(Approval):
    expenses = models.ForeignKey(Expense, related_name='expense_approvals')
    
class ExpenseApprovalRequest(ApprovalRequest):
    expenses = models.ForeignKey(Expense, related_name='expense_approval_requests')

class JourneyApproval(Approval):
    journey = models.ForeignKey(Journey, related_name='journey_approvals')

class JourneyApprovalRequest(ApprovalRequest):
    journey = models.ForeignKey(Expense, related_name='journey_approval_requests')

