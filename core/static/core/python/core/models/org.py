from django.contrib.auth.models import User
from django.db import models


class Employee(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE, blank=True)
    departments = models.ManyToManyField('Department', related_name='users', blank=True)
    bank_account = models.CharField(max_length=250, blank=True, null=True)
    
    def __str__(self):
        return self.user.first_name+" "+self.user.last_name+ " ("+self.user.email+")"

class Department(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=10, blank=True, null=True)
    head = models.ForeignKey('core.Employee', related_name='departments_headed_by_me')
    vice_head = models.ForeignKey('core.Employee', blank=True, null=True, related_name='departments_viceheaded_by_me')
    parent = models.ForeignKey('core.Department', related_name='children', blank=True, null=True)
    
    def __str__(self):
        return self.name+" ("+self.short_name+")"

