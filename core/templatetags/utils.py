from django.utils.safestring import mark_safe

from django_jinja import library

jinja_tag = library.global_function

@jinja_tag
def icon(icon_name):
    """
        {{ fa_icon("address-book") %} renders as <span class='icon fa fa-address-book'></span>
    """
    return mark_safe("<span class='icon fa fa-{icon}'></span>".format(icon=icon_name))

