from django.contrib.postgres import fields as psql
from django.db import models


from .org import Employee

class Journey(models.Model):
    employee = models.ForeignKey(Employee, related_name='journeys')

class JourneySegment(models.Model):
    journey = models.ForeignKey(Journey, related_name='segments')
    start = models.DateTimeField()
    end = models.DateTimeField()
    country = models.ForeignKey('Country')

   
class Currency(models.Model):
    class Meta:
        verbose_name_plural = "Currencies"
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=10, primary_key=True)
    
    def __str__(self):
        return self.name+" ("+self.short_name+")"
    


class Country(models.Model):
    class Meta:
        verbose_name_plural = "Countries"
    name = models.CharField(max_length=50, primary_key=True)
    perdiems = psql.JSONField(default=dict)
    currency = models.ForeignKey(Currency)
    
    def __str__(self):
        return self.name+" ("+self.currency.short_name+")"

