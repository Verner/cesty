from django.db import models
import os
from time import strftime

from .journeys import Journey, Currency
from .org import Employee

class Account(models.Model):
    number = models.IntegerField()
    title = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=250,blank=True, null=True)
    owners = models.ManyToManyField(Employee, blank=True)
    balance = models.FloatField()
    
    def __str__(self):
        ret = str(self.number)
        if self.title:
            ret+= " "+self.title
        ret += "("+ self.balance +" CZK)"

class Expense(models.Model):
    TYPE = (
        ('1-perdiem', 'Per diem (kapesné + stravné)'),
        ('2-travel', 'Doprava'),
        ('3-registrationfee', 'Poplatky (registrační poplatky na konferenci, ...)'),
        ('4-other', 'Jiné'),
    )
    journey = models.ForeignKey(Journey)
    accounts = models.ManyToManyField(Account, through='AccountExpense', related_name='expenses')
    expense_type = models.CharField(choices=TYPE, max_length=250, default='4-other')
    expense_description = models.CharField(max_length=500, blank=True, null=True)
    expected_amount = models.FloatField(blank=True, null=True)
    actual_amount = models.FloatField(blank=True, null=True)
    currency = models.ForeignKey(Currency)
    finalized = models.BooleanField(default=False)
    
class AccountExpense(models.Model):
    expense = models.ForeignKey(Expense)
    account = models.ForeignKey(Account)
    amount_czk = models.FloatField()

class Document(models.Model):
    
    def file_name(self, instance, filename):
        fmt = strftime('receipts/%Y/%m/{department}/{employee}-{journey}-{expense}-{id}.{ext}')
        name, ext = os.path.splitext(filename)
        return fmt.format(department=instance.expense.journey.employee.department_set[0],
                          employee=instance.expense.journey.employee.user.lastname,
                          journey=instance.expense.journey,
                          expense=instance.expense,
                          ext=ext,
                          id=self.id)

    expense = models.ForeignKey(Expense)
    description = models.CharField(max_length=250,blank=True, null=True)
    file = models.FileField(upload_to=file_name)
    

