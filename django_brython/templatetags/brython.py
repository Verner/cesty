import os

from django import template
from django.template.loader import render_to_string
from django.templatetags.static import static
from django.utils.functional import LazyObject
from django.utils.safestring import mark_safe


try:
    
    from django_jinja import library
    jinja_tag = library.global_function
except:
    def jinja_tag(func):
        return func

register = template.Library()


@register.simple_tag()
@jinja_tag
def python_path(additional_paths=''):
    PATHS = [
        'django_brython/python'
    ]
    if len(additional_paths) > 0:
        PATHS.extend(additional_paths.split(':'))
    PATHS = map(static,PATHS)
    ret = []
    for p in PATHS:
        ret.append("<link rel='pythonpath' href='{path}' hreflang='py' />".format(path=p))
    return mark_safe('\n'.join(ret))


@register.simple_tag()
@jinja_tag
def brython_app(app_name, entry_point, *args, **kwargs):
    PYTHON_PATH = app_name+'/python'
    ENTRY_POINT = PYTHON_PATH+'/'+app_name+'/'+entry_point
    if ':' in entry_point:
        module, func = entry_point.split(':')
        fargs = []
        for a in args:
            if isinstance(a, LazyObject):
                a._setup()
                a = a._wrapped
            if isinstance(a, str):
                a='"""'+a+'"""'
            fargs.append(a)
        for name, val in kwargs.items():
            if isinstance(val, LazyObject):
                val._setup()
                val = val._wrapped
            if isinstance(val, str):
                val='"""'+val+'"""'
            fargs.append(name+"="+val)
        fargs = mark_safe(",".join([str(a) for a in fargs]))
        imp = """
        from {app}.{module} import {func}
        {func}({fargs})
"""
    else:
        fargs=""
        func="pass"
        module=""
        imp = """
        import {app}.{entry_point}
"""
    html = ("""
<!--<link rel='pythonpath' href='{path}'/>-->

<script type='text/python'>
    import sys, os, logging
    
    sys.path.append('{path}')
    
    try:
        import {app}.settings as app_settings
        from brdj.django.conf import register_settings
        register_settings(app_settings)
    except:
        pass
    try:
"""+imp+"""
    except Exception as ex:
        from browser import console
        console.log("Error running {app}.{entry_point}")
        console.log("Exception: "+str(ex))
        pass
</script>
    """).format(app=app_name, 
               path=static(PYTHON_PATH),
               entry_point=entry_point,
               main=static(ENTRY_POINT+'.py'),
               func=func,
               imp=imp,
               module=module,
               fargs=fargs)
    return mark_safe(html)
        

@register.simple_tag()
@jinja_tag
def brython_static(asset_type=None):
    ctx = {'asset_type':asset_type}
    return mark_safe(render_to_string('django_brython/static.html', context=ctx))


