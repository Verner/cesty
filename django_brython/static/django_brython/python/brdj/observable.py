import copy
import functools
import logging

from .events import EventSource

logger = logging.getLogger(__name__)


class ObservableDict(EventSource):
    def __init__(self, data=None):
        super().__init__()
        self._data = {}
        if data is not None:
            for key, val in data.items():
                if isinstance(val, dict):
                    v = ObservableDict(val)
                elif isinstance(val, list):
                    v = ObservableList(val)
                else:
                    v = val
                if hasattr(v, 'bind'):
                    v.bind('change', functools.partial(self._change, key=key))
                self._data[key] = v

    def __getitem__(self, key):
        return self._data[key]

    def items(self):
        return self._data.items()

    def keys(self):
        return self._data.keys()

    def values(self):
        return self._data.values()

    def __contains__(self, key):
        return key in self._data

    def __setitem__(self, key, val):
        self._data[key] = val
        self.emit('changed', {'key': key})

    def __delitem__(self, key):
        del self._data[key]
        self.emit('changed', {'key': key})

    def _changed(self, key=None, event=None):
        self.emit('changed', {key: key})


class ObservableList(EventSource):
    def __init__(self, data=None):
        super().__init__()
        if data is None:
            self._data = []
        else:
            self._data = copy.deepcopy(list(data))

    def append(self, obj):
        self._data.append(obj)
        self.emit('insert',{'index': len(self._data), 'obj': obj})
        self.emit('changed')

    def clear(self):
        self._data.clear()
        self.emit('clear')
        self.emit('changed')

    def sort(self, *args, **kwargs):
        self._data.sort(*args, **kwargs)
        self.emit('sort')
        self.emit('changed')

    def insert(self, index, obj):
        self._data.insert(index, obj)
        self.emit('insert', {'index': index, 'obj': obj})
        self.emit('changed')

    def extend(self, lst):
        first = len(self._data)
        if isinstance(lst, list):
            self._data.extend(lst)
        else:
            raise NotImplementedError()
        self.emit('extend', {'first': first})
        self.emit('changed')

    def remove(self, item):
        for idx, it in enumerate(self._data):
            if it == item:
                del self[idx]
                return
        raise ValueError(item+" not in list")

    def __contains__(self, val):
        return val in self._data

    def __getitem__(self, key):
        return self._data[key]

    def __getslice__(self, i, j):
        return ObservableList(self._data[i:j])

    def __len__(self):
        return len(self._data)

    def __add__(self, other):
        new = ObservableList(self)
        new.extend(other)
        return new

    def __del__(self, index):
        self.emit('delete', {'index':index})
        del self._data[index]

    def __iter__(self):
        for d in self._data:
            yield d

