from logging import getLogger

logger = getLogger(__name__)

class EventSource:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._event_handlers = {}

    def bind(self, ev_name, handler):
        if ev_name not in self._event_handlers:
            self._event_handlers[ev_name] = []
        handlers = self._event_handlers[ev_name]
        handlers.append(handler)

    def emit(self, ev_name, ev_data=None):
        if ev_data is None:
            ev_data = {}
        handlers = self._event_handlers.get(ev_name, [])
        ev_data['event_name'] = ev_name
        for h in handlers:
            try:
                h(ev_data)
            except Exception as ex:
                logger.error("Handler: %s raised exception %s when handling %s", str(h), str(ex), str(ev_data))
