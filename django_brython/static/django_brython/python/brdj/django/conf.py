class Settings:
    def __init__(self):
        self._dict = {}

    def __getattr__(self, name):
        return self._dict[name]

    def __setattr__(self, name, value):
        if name.startswith('_'):
            super().__setattr__(name, value)
        self._dict[name] = value

settings = Settings()


def register_settings(self, module):
    for attr in dir(module):
        setattr(settings, attr, getattr(module, attr))

