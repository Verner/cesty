import brdj.django.db.models as models


class User(models.Model):
    firstname = models.CharField(max_length=255, blank=True)
    lastname = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)

models.register(User)

