import json
import logging

from brdj.observable import ObservableDict, ObservableList
import asyncio


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


SET_NULL = 0
CASCADE = 0


class ModelField:
    def __init__(self, title='', required='', help='', null=False, blank=False, default=None, readonly=False):
        self._title = title
        self._required = required
        self._help = help
        self._default = default
        self._readonly = readonly


class ForeignKey(ModelField):
    def __init__(self, model, on_delete=SET_NULL, related_name=None, **kwargs):
        super().__init__(**kwargs)
        self._foreign_model = model

class OneToOneField(ForeignKey):
    pass

class ManyToManyField(ForeignKey):
    def __init__(self, model, through=None, **kwargs):
        super().__init__(model, **kwargs)



class CharField(ModelField):
    def __init__(self, max_length, choices=None, **kwargs):
        super().__init__(**kwargs)
        self._max_length = max_length
        self._choices = choices


class URLField(ModelField):
    pass


class TextField(ModelField):
    pass


class OpaqueField(ModelField):
    def __init__(self, **kwargs):
        kwargs['readonly'] = True
        super().__init__(**kwargs)


class Model(ObservableDict):
    API_URL = '/api'
    MODEL = None
    URL = None

    objects = None

    class ModelError(Exception):
        def __init__(self, msg):
            super().__init__(msg)

    @classmethod
    def _fields(cls):
        for fld_name in dir(cls):
            fld = getattr(cls, fld_name)
            if isinstance(fld, ModelField):
                yield (fld_name, fld)

    @classmethod
    def _have_field(cls, field_name):
        if not hasattr(cls, field_name):
            return False
        fld = super().__getattribute__(cls, field_name)
        return isinstance(fld, ModelField)

    @classmethod
    def object_factory(model_cls):
        class objects:
            _OBJECTS = ObservableList()
            _OBJECTS_BY_ID = {}
            _NEW_OBJECTS = ObservableList()
            _LOADED = None

            @classmethod
            def _register(cls, instance):
                logger.info("Registering an instance of %s", str(model_cls))
                logger.info("  instance: %s", str(instance))
                if instance.id is not None:
                    if instance.id in cls._OBJECTS_BY_ID:
                        raise Model.ModelError("Object with id '"+instance.id+"' exists.")
                    else:
                        cls._OBJECTS_BY_ID[instance.id] = instance
                        cls._OBJECTS.append(instance)
                else:
                    cls._OBJECTS.append(instance)
                    cls._NEW_OBJECTS.append(instance)

            @classmethod
            @asyncio.coroutine
            def load(cls):
                if isinstance(cls._LOADED, asyncio.Future):
                    return
                cls._LOADED = asyncio.Future()
                logger.warn("Loading..." + str(model_cls) + " "+str(model_cls._list_url()))
                req = yield from asyncio.HTTPRequest(model_cls._list_url())
                logger.info("Got response...")
                cls._OBJECTS_BY_ID = {}
                cls._OBJECTS.clear()
                objects = json.loads(req.response)
                try:
                    for obj in objects:
                        o_obj = model_cls(obj)
                        logger.info("Obj:"+ str(o_obj))
                except Exception as e:
                    logger.warn("Exception: %s"+str(e))
                logger.warn("Loaded %s" +str(model_cls))
                cls._LOADED.set_result(None)

            @classmethod
            @asyncio.coroutine
            def wait_for_load(cls):
                cls.load()
                yield asyncio.wait_for(cls._LOADED)

            @classmethod
            def new(cls):
                return cls._NEW_OBJECTS

            @classmethod
            def all(cls):
                logger.info("Getting All objects for "+str(model_cls))
                asyncio.ensure_future(cls.load())
                return cls._OBJECTS

            @classmethod
            def get(cls, **kwargs):
                if 'id' in kwargs:
                    try:
                        return cls._OBJECTS_BY_ID[kwargs['id']]
                    except:
                        raise Model.ModelError("No Object found for query: "+str(kwargs))
                for obj in cls._OBJECTS:
                    matches = True
                    for key, val in kwargs:
                        if not obj[key] == val:
                            matches = False
                            break
                    if matches:
                        return obj
                raise Model.ModelError("No Object found for query: "+str(kwargs))
        return objects

    @classmethod
    def _model(cls):
        if cls.MODEL is None:
            cls.MODEL = cls.__name__
        return cls.MODEL

    @classmethod
    def _url(cls):
        if cls.URL is None:
            cls.URL = cls._model().lower()
        return cls.URL

    @classmethod
    def _create_url(cls):
        return '/'.join([cls.API_URL, cls._url()])+'/'

    @classmethod
    def _list_url(cls):
        return '/'.join([cls.API_URL, cls._url()])+'/'

    def _update_url(self):
        return '/'.join([self.API_URL, self._url(), self.id])+'/'

    def _make_dirty(self):
        self._dirty = True

    def __getattribute__(self, name):
        fld = super().__getattribute__(name)
        if not isinstance(fld, ModelField):
            return fld
        if name in self:
            return self[name]
        if fld._default:
            return fld._default
        return None

    def __setattr__(self, name, val):
        fld = super().__getattribute__(name)
        if not isinstance(fld, ModelField):
            super().__setattr__(name, val)
        if fld._readonly:
            raise Model.ModelError("Readonly field")
        else:
            self[name] = val

    def field_values(self):
        ret = {}
        for (fld, fld_name) in self._fields():
            if fld_name in self:
                ret[fld_name] = self[fld_name]
            elif fld._default:
                ret[fld_name] = fld._default
        return ret

    def _set_fld_value(self, name, val):
        if not self._have_field(name) and not name == 'id':
            logger.warn("WARNING: Unexpected field '"+name+"'")
        else:
            self[name] = val

    def __init__(self, data=None):
        super().__init__()
        self._dirty = False
        self._deleted = False
        if data is not None:
            for k, v in data.items():
                self._set_fld_value(k, v)
        self.objects._register(self)
        self.bind('change', self._make_dirty)

    @property
    def id(self):
        if 'id' in self:
            return self['id']
        else:
            return None

    @property
    def status(self):
        if self._deleted:
            return 'deleted'
        if self.id is None:
            return 'new'
        if self._dirty:
            return 'dirty'
        return 'clean'

    def clean(self):
        pass

    @asyncio.coroutine
    def delete(self):
        if self.status == 'new':
            self._deleted = True
            self.objects._NEW_OBJECTS.remove(self)
            self.objects._OBJECTS.remove(self)
        else:
            self._deleted = True
            req = yield from asyncio.HTTPRequest(self._update_url(), method='DELETE')
            self.objects._OBJECTS.remove(self)

    @asyncio.coroutine
    def save(self):
        status = self.status
        if status == 'clean':
            return
        if status == 'deleted':
            raise Model.ModelError("Cannot save deleted model")
        if status == 'new':
            url = self._create_url()
            create = True
        else:
            url = self._update_url()
            create = False
        self.clean()
        req = yield from asyncio.HTTPRequest(url, data=self.field_values())
        data = json.loads(req.response)
        for k, v in data.items():
            self._set_fld_value(k, v)
        if create:
            self.objects._NEW_OBJECTS.remove(self)
            self.objects._OBJECTS_BY_ID[self.id] = self
        self._dirty = False


class OpaqueModel(Model):

    def _set_fld_value(self, name, val):
        if not name == 'id':
            logger.warn("WARNING: Setting value on an OpaqueModel'"+name+"'")
        else:
            self[name] = val

    def __init__(self, data=None):
        super().__init__()
        self._dirty = False
        self._deleted = False
        if 'id' not in data:
            raise Model.ModelError("Cannot create new instance of an OpaqueModel")
        else:
            self['id'] = data['id']
        self.objects._register(self)

    @property
    def id(self):
        if 'id' in self:
            return self['id']
        else:
            return None

    @property
    def status(self):
        if self._deleted:
            return 'deleted'
        return 'clean'

    @asyncio.coroutine
    def delete(self):
        self._deleted = True
        req = yield from asyncio.HTTPRequest(self._update_url(), method='DELETE')
        self.objects._OBJECTS.remove(self)

    @asyncio.coroutine
    def save(self):
        logger.warn("WARNING: Cannot save an OpaqueModel")


def register(cls):
    cls.objects = cls.object_factory()

    # Resolve ForeignKeys referenced by string
    for _, fld in cls._fields():
        if isinstance(fld, ForeignKey) and type(fld._foreign_model) == str:
            # FIXME: For now we only handle autoreferences
            fld._foreign_model = cls

