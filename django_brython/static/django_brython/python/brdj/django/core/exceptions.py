class DjangoException(BaseException):
    pass


class ValidationError(DjangoException):
    pass


class PermissionDenied(DjangoException):
    pass
