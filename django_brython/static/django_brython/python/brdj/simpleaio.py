"""
    A module providing the :class:`Promise` and :function:`async` decorator
    which can be make async operations look like sync. Typical usecase is
    as follows. Assume we have a method ``query_server`` which asynchronously
    queries a server. So, instead of returning the results, it returns an
    instance of the :class:`Promise` class. Normally one would call
    the :method:`then` method of this instance providing it with a call back
    to be called when the promise is "resolved", i.e. when the query has
    finished and results are ready. This, however, typically leads to something
    called the "callback" hell. The :function:`async` decorator can get
    around this by a clever use of the ``yield`` statement. So, instead of
    writing:

    ```
        def process_results(results):
            do_some_stuff(results)

        def send_query():
            promise = query_server()
            promise.then(process_results)
    ```

    one can write it in a more straightforward way:

    ```
        @async
        def process_query():
            results = yield query_server()
            do_some_stuff(results)
    ```

    eliminating the need to introduce the ``process_results`` callback.
"""
from browser import ajax
from browser import timer

from logging import getLogger
from time import time

logger = getLogger(__name__)  # pylint: disable=invalid-name


class InvalidStateError(Exception):
    pass

class CancelledError(Exception):
    pass


class Handle:
    def __init__(self, timeout):
        self._timeout = timeout

    def cancel(self):
        timer.clear_timeout(self._timeout)

class BrowserEventLoop:
    def run_forever(self):
        pass

    def run_until_complete(self, future):
        raise NotImplementedError

    def is_running(self):
        return True

    def call_soon(self, callback, *args):
        def _callback():
            callback(*args)
        return Handle(timer.set_timeout(_callback,0))

    def call_later(self, delay, callback, *args):
        def _callback():
            callback(*args)
        return Handle(timer.set_timeout(_callback,delay*1000))

    def call_at(self, when, callback, *args):
        def _callback():
            callback(*args)
        return Handle(timer.set_timeout(_callback,when-self.time()))

    def time(self):
        return time.time()

_event_loop = BrowserEventLoop()

def get_event_loop():
    return _event_loop


class Future:
    """
        A class representing the future result of an async action.
        Implementations should override the :method:`start` method
        which should start the asynchronous operation. The class will
        typically register a handler to be run when the operation finishes.
        This handler then needs to call the base :methdo:`_finish` method
        providing it with the :parameter:`result` parameter and
        :parameter:`status` (which should either be ``Promise.STATUS_FINISHED``
        in case the operation finished successfully or ``Promise.STATUS_ERROR``
        if an error happened).
    """
    STATUS_STARTED = 0
    STATUS_CANCELED = 1
    STATUS_FINISHED = 2
    STATUS_ERROR = 3

    def __init__(self, loop=None):
        if loop is None:
            self._loop = get_event_loop()
        else:
            self._loop = None
        self._status = Future.STATUS_STARTED
        self._result = None
        self._exception = None
        self._callbacks = []

    def _schedule_callbacks(self):
        for cb in self._callbacks:
            self._loop.call_soon(cb, self)

    def cancel(self):
        """
        Cancel the future and schedule callbacks.

        If the future is already done or cancelled, return False. Otherwise, change the future’s state to cancelled, schedule the callbacks and return True."""
        if self._status != Future.STATUS_STARTED:
            return False
        self._status = Future.STATUS_CANCELED
        self._schedule_callbacks()
        return True


    def cancelled(self):
        """Return True if the future was cancelled."""
        return self._status == Future.STATUS_CANCELED

    def done(self):
        """
        Return True if the future is done.

        Done means either that a result / exception are available, or that the future was cancelled.
        """
        return self._status != Future.STATUS_STARTED

    def result(self):
        """
        Return the result this future represents.

        If the future has been cancelled, raises CancelledError. If the future’s result isn’t yet available, raises InvalidStateError. If the future is done and has an exception set, this exception is raised.
        """
        if self._status == Future.STATUS_STARTED:
            raise InvalidStateError()
        if self._status == Future.STATUS_CANCELED:
            raise CancelledError()
        if self._status == Future.STATUS_ERROR:
            raise self._exception
        return self._result


    def exception(self):
        """
        Return the exception that was set on this future.

        The exception (or None if no exception was set) is returned only if the future is done. If the future has been cancelled, raises CancelledError. If the future isn’t done yet, raises InvalidStateError.
        """
        if self._status == Future.STATUS_STARTED:
            raise InvalidStateError()
        if self._status == Future.STATUS_CANCELED:
            raise CancelledError()
        if self._status == Future.STATUS_ERROR:
            return self._exception

    def add_done_callback(self, fn):
        """
        Add a callback to be run when the future becomes done.

        The callback is called with a single argument - the future object. If the future is already done when this is called, the callback is scheduled with call_soon().

        Use functools.partial to pass parameters to the callback. For example, fut.add_done_callback(functools.partial(print, "Future:", flush=True)) will call print("Future:", fut, flush=True).
        """
        if self.done():
            self._loop.call_soon(fn,self)
        else:
            self._callbacks.append(fn)

    def remove_done_callback(self, fn):
        """
        Remove all instances of a callback from the “call when done” list.

        Returns the number of callbacks removed.
        """
        removed = 0
        retain = []
        for cb in self._callbacks:
            if cb == fn:
                removed += 1
            else:
                retain.append(cb)
        self._callbacks = retain
        return removed

    def set_result(self, result):
        """
        Mark the future done and set its result.

        If the future is already done when this method is called, raises InvalidStateError.
        """
        if self._status != Future.STATUS_STARTED:
            raise InvalidStateError()
        self._result = result
        self._status = Future.STATUS_FINISHED
        self._schedule_callbacks()

    def set_exception(self, exception):
        """
        Mark the future done and set an exception.

        If the future is already done when this method is called, raises InvalidStateError.
        """
        if self._status != Future.STATUS_STARTED:
            raise InvalidStateError()
        self._exception = exception
        self._status = Future.STATUS_ERROR
        self._schedule_callbacks()

def get_continuation(generator, final_result):
    def _cb(previous_result):
        if final_result.cancelled():
            return
        try:
            if previous_result.cancelled():
                next_result = generator.throw(CancelledError())
            elif previous_result.exception() is not None:
                next_result = generator.throw(previous_result.exception())
            else:
                next_result = generator.send(previous_result.result())
            cb = get_continuation(generator, final_result)
            next_result.add_done_callback(cb)
        except StopIteration:
            final_result.set_result(None)
        except Exception as ex:
            final_result.set_exception(ex)
    return _cb


def decorator(dec):
    def new_dec(fn):
        ret = dec(fn)
        ret.__decorated = fn
        return ret
    return new_dec

@decorator
def coroutine(func):
    """
        A coroutine decorator which allows a function to call asynchronous operations
        (using "yield") and use their results as if they were synchronous operations,
        e.g.

        @coroutine
        def print_google()
            html = yield wget("www.google.com")
            print(html)

        would download the www.google.com website and then print its html. The magic
        is that the ``wget`` function returns a Future, but the @async generator
        converts this promise into an actual value which is sent back to the function
        so that when ``print`` is called it has the results ready.
    """

    def run(*args, **kwargs):
        generator = func(*args, **kwargs)
        final_result = Future()
        try:
            first_result = next(generator)
            _cb = get_continuation(generator, final_result)
            first_result.add_done_callback(_cb)
        except StopIteration:
            final_result.set_result(None)
        except Exception as ex:
            final_result.set_exception(ex)
        return final_result
    run.__coroutine = True
    return run


def wait_for(future):
    return future

def ensure_future(val):
    if isinstance(val, Future):
        ret = val
    else:
        ret = Future()
        ret.set_result(val)
    return ret

class AsyncObjectInitCanceledError(Exception):
    def __str__(self):
        return "Async object initialization canceled"

class AsyncObjectInitExceptionError(Exception):
    def __init__(self, ex):
        super().__init__("Init threw an exception:"+str(ex))
        self._init_ex = ex

class AsyncObjectUninitializedError(Exception):
    def __str__(self):
        return "Async object not initialized"

@decorator
def async_init(init):
    """
        A decorator for asynchronous constructors.
    """
    def new_init(self, *args, **kwargs):
        logger.debug("Calling decorated init for %s", self.__class__.__name__)
        cls = str(self.__class__)
        def _init_cb(fut):
            if fut.cancelled():
                logger.error("Constructor for %s canceled.", cls)
            elif fut.exception() is not None:
                print(fut.exception())
                logger.error("Constructor for %s raised an exception: %s", cls, str(fut.exception()))
        self._init_future = coroutine(init)(self, *args, **kwargs)
        self._init_future.add_done_callback(_init_cb)

    return new_init



def defer(future, func, *args, **kwargs):
    ret = Future()
    def cb(fut):
        if fut.canceled():
            logger.error("Unable to call deferred method %s object init canceled.", str(func.__decorated))
            ret.set_exception(AsyncObjectInitCanceledError())
        if fut.exception() is not None:
            logger.error("Unable to call deferred method %s object init raised an exception:", str(fut.exception))
            ret.set_exception(AsyncObjectInitExceptionError(fut.exception()))
        else:
            try:
                ret.set_result(func(*args, **kwargs))
            except Exception as ex:
                ret.set_exception(ex)
    future.add_done_callback(cb)
    return ret

@decorator
def _generate_guard(func):
    def guard(self, *args, **kwargs):
        if self._init_future.cancelled():
            logger.error("Unable to call deferred method %s object init canceled.", str(func))
            raise AsyncObjectInitCanceledError()
        elif self._init_future.exception() is not None:
            raise AsyncObjectInitExceptionError(self._init_future.exception())
        elif self._init_future.done():
            return func(self, *args, **kwargs)
        else:
            if hasattr(func, '__coroutine'):
                logger.info("Defering method %s until object is initialized.", str(func))
                return defer(self._init_future, func, self, *args, **kwargs)
            else:
                logger.error("Calling method on Uninitialized object")
                raise AsyncObjectUninitializedError()
    return guard


def async_class(cls):
    """
        A decorator for classes having an asynchronous constructor.
        Care must be given that all method invocations must be deferred until
        the constructor has finished. This is done by the :function:`_generate_guard`
        function which is called on each nonprivate class method.
    """
    for member in dir(cls):
        if not member[0:2] == '__':
            meth = getattr(cls, member)
            if hasattr(meth, '__call__'):
                setattr(cls, member, _generate_guard(meth))
    return cls


class HTTPException(Exception):
    """
        A class representing a HTTPRequest error
    """
    def __init__(self, request):
        super(HTTPException, self).__init__()
        self.req = request


class HTTPRequest(Future):
    """
        A class representing a Future HTTPRequest result.
    """
    METHOD_POST = 'POST'
    METHOD_GET = 'GET'

    def __init__(self, url, method='GET', data=None, **kwargs):
        super(HTTPRequest, self).__init__(**kwargs)
        self._url = url
        self._req = ajax.ajax()
        self._req.bind("complete", self._complete_handler)
        self._data = data
        self._method = method
        self._req.open(self._method, self._url, True)
        self._req.set_header('content-type', 'application/x-www-form-urlencoded')
        if self._data is None:
            self._req.send()
        else:
            self._req.send(self._data)

    def cancel(self):
        self._req.abort()
        super().cancel()

    def _complete_handler(self, req):
        if req.status == 200 or req.status == 0:
            self.set_result(req)
        else:
            self.set_exception(HTTPException(req))
