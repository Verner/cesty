from browser import timer, html, window, document as doc, console
from functools import partial

from brdj.events import EventSource


def animate(start_val, stop_val, action, duration, start_action=None, end_action=None):
    previous_time = None
    next_val = start_val
    delta = (stop_val-start_val)/duration

    def animation_frame(time_stamp):
        nonlocal previous_time, delta, next_val
        if previous_time is None:
            if start_action:
                start_action()
            previous_time = time_stamp
            timer.request_animation_frame(animation_frame)
            return
        action(next_val)
        next_val += delta*(time_stamp-previous_time)
        previous_time = time_stamp
        if (delta > 0 and next_val < stop_val) or (delta < 0 and next_val > stop_val):
            timer.request_animation_frame(animation_frame)
        else:
            action(stop_val)
            if end_action:
                end_action()
    timer.request_animation_frame(animation_frame)


class ReferencedObject:
    ALL_OBJECTS = {}
    LAST_ID = 0

    @classmethod
    def _next_id(cls):
        cls.LAST_ID += 1
        return cls.LAST_ID

    @classmethod
    def get_object(cls, id):
        return cls.ALL_OBJECTS[id]

    def __init__(self, *args, **kwargs):
        self._object_id = ReferencedObject._next_id()
        ReferencedObject.ALL_OBJECTS[self._object_id] = self


class WidgetException(Exception):
    pass


class CSSClassList:
    def __init__(self, element):
        self._element = element

    def append(self, css_class):
        self._element.class_name += ' '+css_class

    def remove(self, css_class):
        self._element.class_name = self._element.class_name.replace(css_class, '')

    def extend(self, css_class_list):
        self._element.class_name += ' '+' '.join(css_class_list)

    def __contains__(self, css_class):
        return css_class in self._element.class_name

    def __iter__(self):
        for css_class in self._element.class_name.split(' '):
            if not css_class == "":
                yield css_class

    def __len__(self):
        return len([c for c in self._element.class_name.split(' ') if not c == ""])


class Widget(ReferencedObject, EventSource):
    DEFAULT_CSS_CLASS = 'widget'
    DEFAULT_TAG_NAME = 'div'
    HTML_TEMPLATE = ''
    EVENTS = ['click', 'focus', 'blur', 'keydown', 'keypress', 'keyup', 'mousemove', 'mouseleave', 'mouseout']

    @classmethod
    def element_to_widget(cls, elt):
        for obj in cls.ALL_OBJECTS:
            if obj.element == elt:
                return obj
        return None

    def _install_handlers(self, element):
        for c in element:
            for ev in self.EVENTS:
                if hasattr(c, 'getAttribute'):
                    h = c.getAttribute('on'+ev)
                    if h:
                        c.removeAttribute('on'+ev)
                        c.bind(ev, getattr(self, h))
            self._install_handlers(c)

    def _scoped_id(self, id):
        return 'widget:'+str(self._object_id)+':'+id

    def find_child(self, id, root=None):
        if root is None:
            root = self.element
        for c in root:
            if c.id == self._scoped_id(id):
                return c
            sc = self.find_child(id, root=c)
            if sc is not None:
                return sc
        return None

    def bind(self, ev_name, handler):
        if ev_name in self.EVENTS:
            self.element.bind(ev_name, handler)
        else:
            super().bind(ev_name, handler)

    def __init__(self, tag_name=None, element=None, *args, **kwargs):
        super().__init__()
        self._event_handlers = {}
        self.element = None
        self.children = []
        self.parent = None
        if element is not None:
            self.element = element
        else:
            if tag_name is None:
                tag_name = self.DEFAULT_TAG_NAME
            inner_html = self.HTML_TEMPLATE.replace(
                'id="', 'id="'+'widget:'+str(self._object_id)+':').replace(
                "id='", "id='"+'widget:'+str(self._object_id)+':').replace(
                "main_element", "id='"+'widget:'+str(self._object_id)+':main_element'+"' ")
            self.element = getattr(html, tag_name.upper())(inner_html)
            if 'main_element' in self.HTML_TEMPLATE:
                self.main_element = self.find_child('main_element')
            else:
                self.main_element = self.element
        if len(self.element.id) == 0:
            self.element.id = 'widget:'+str(self._object_id)
        self._install_handlers(self.element)
        self._css_class_list = CSSClassList(self.element)
        self.css_classes.append('w-'+self.DEFAULT_CSS_CLASS)

    def reparent(self, parent):
        if isinstance(parent, Widget):
            parent.append_child(self)
        else:
            parent.appendChild(self.element)

    def __le__(self, other):
        if isinstance(other, Widget):
            return self.append_child(other)
        else:
            return self.main_element <= other

    @property
    def css_classes(self):
        return self._css_class_list

    @property
    def id(self):
        return self._object_id

    @property
    def pixel_width(self):
        return self.element.elt.clientWidth

    @property
    def pixel_height(self):
        return self.element.elt.clientHeight

    @property
    def screen_pos(self):
        return self.element.offsetTop, self.element.offsetLeft

    def client_to_widget(self, position):
        x, y = position
        rect = self.element.elt.getBoundingClientRect()
        return x-rect.left, y-rect.top

    def on_add_child(self, child):
        """
            Called whenever widget has a new child.
        """
        pass

    def on_remove_child(self, child):
        """
            Called whenever a child widget is removed
        """
        pass

    def append_child(self, child_widget):
        if child_widget in self.children:
            raise WidgetException("Cannot append widget twice.")
        if child_widget.parent:
            child_widget.parent.remove_child(child_widget)
        self.children.append(child_widget)
        self.main_element.elt.appendChild(child_widget.element.elt)
        self.on_add_child(child_widget)
        child_widget.parent = self

    def insert_child(self, child_widget, before):
        if not before:
            self.append_child(child_widget)
        else:
            if child_widget in self.children:
                raise WidgetException("Cannot insert widget twice.")
            if child_widget.parent:
                child_widget.parent.remove_child(child_widget)
            self.main_element.elt.insertBefore(child_widget.element.elt, before.element.elt)
            self.children.insert(self.children.index(before), child_widget)
            self.on_add_child(child_widget)
            child_widget.parent = self

    def remove_child(self, child_widget):
        """
            Removes the child and returns the widget immediately
            preceding it.
        """
        if child_widget not in self.children:
            raise WidgetException("Cannot remove non-child widget.")
        try:
            idx = self.children.index(child_widget)
            self.main_element.elt.removeChild(child_widget.element.elt)
            self.children.remove(child_widget)
            child_widget.parent = None
            self.on_remove_child(child_widget)
            if idx > 0:
                return self.children[idx-1]
            else:
                return None
        except Exception as ex:
            raise WidgetException("Error when removing child:"+str(ex))

    def clear_children(self):
        for ch in self.children:
            self.main_element.elt.removeChild(ch.element.elt)
            ch.parent = None
            self.on_remove_child(ch)
        self.children = []

    def hide(self):
        self.element.style.display = 'none'

    def show(self):
        self.element.style.display = 'initial'


class IconTheme(object):
    def _get_class(self, icon_name):
        return 'icon'

    def icon(self, icon_name):
        return Icon(theme=self)


class FontAwesomeTheme(IconTheme):
    def _get_class(self, icon_name):
        return 'icon fa fa-'+icon_name


class Icon(Widget):
    DEFAULT_TAG_NAME = 'span'
    DEFAULT_ICON_THEME = FontAwesomeTheme()

    def _get_class(self):
        return 'icon fa fa-'+self.icon_name

    def __init__(self, icon_name, **kwargs):
        super().__init__(**kwargs)
        self.theme = kwargs.get('theme', self.DEFAULT_ICON_THEME)
        self.name = icon_name
        self.element.class_name = self.theme._get_class(icon_name)


class Button(Widget):
    DEFAULT_CSS_CLASS = 'button'
    DEFAULT_TAG_NAME = 'button'

    def __init__(self, text, *args, **kwargs):
        super().__init__()
        self.element.innerHTML = text
        self.css_classes.append('btn')

    @property
    def title(self):
        return self.element.textContent


class PrimaryButton(Button):
    def __init__(self, text, *args, **kwargs):
        super().__init__(text, *args, **kwargs)
        self.css_classes.append('btn-primary')


class OkButton(Button):
    def __init__(self, text='Ok', *args, **kwargs):
        super().__init__(text, *args, **kwargs)
        self.css_classes.append('btn-primary')


class CancelButton(Button):
    def __init__(self, text='Cancel', *args, **kwargs):
        super().__init__(text, *args, **kwargs)
        self.css_classes.append('btn-danger')


class ButtonGroup(Widget):
    DEFAULT_CSS_CLASS = 'button-group'

    def __init__(self, buttons, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.buttons = buttons
        for btn in self.buttons:
            self.append_child(btn)
            btn.bind('click', partial(self._click_handler, btn))

    def _click_handler(self, btn, ev):
        self.emit('button_clicked', {'btn': btn})


class DnDItem(Widget):
    CURRENT_DRAGGED_ITEMS = {}

    @classmethod
    def get_drag_item(cls, ev):
        for (item_id, pos) in cls.CURRENT_DRAGGED_ITEMS.items():
            if pos == (ev.clientX, ev.clientY):
                return Widget.get_object(item_id)
        return None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.element.elt.draggable = True
        self.element.bind('dragstart', self._drag_start)
        self.element.bind('dragend', self._drag_end)
        self.element.bind('drag', self._drag)
        self._drag_source = None
        self._over_object = None
        self._drag_point_coords = (0, 0)
        self._current_drag_source = None

    @property
    def drag_point(self):
        return self._drag_point_coords

    def _drag(self, ev):
        self.CURRENT_DRAGGED_ITEMS[self.id] = (ev.clientX, ev.clientY)

    def _drag_start(self, ev):
        ev.dataTransfer.setData('text/plain', str(self))
        ev.dataTransfer.setData('x-application/widget-id', str(self.id))
        ev.dataTransfer.effectAllowed = 'move'
        self._drag_point_coords = self.client_to_widget((ev.clientX, ev.clientY))
        if self._drag_source:
            self._drag_source.start_drag(self)
            self._current_drag_source = self._drag_source
        else:
            raise WidgetException("Object not an element of a DragSource")

    def _drag_end(self, ev):
        ev.preventDefault()
        del self.CURRENT_DRAGGED_ITEMS[self.id]
        self._drag_point_coords = (0, 0)
        if self._current_drag_source:
            if ev.dataTransfer.dropEffect == 'none':
                self._current_drag_source.cancel_drag(self)
            else:
                self._current_drag_source.finish_drag(self)
            self._current_drag_source = None


class DnDSource(Widget):
    def on_add_child(self, child_widget):
        super().on_add_child(child_widget)
        if isinstance(child_widget, DnDItem):
            child_widget._drag_source = self

    def start_drag(self, obj):
        pass

    def finish_drag(self, obj):
        pass

    def cancel_drag(self, obj):
        pass


class DnDTarget(Widget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.element.bind('drop', self._drop)
        self.element.bind('dragover', self._drag_over)
        self.element.bind('dragenter', self._drag_enter)
        self.element.bind('dragleave', self._drag_leave)

    def _drop(self, dom_event):
        obj = DnDItem.get_drag_item(dom_event)
        if obj is None:
            return
        if not obj._over_object == self:
            obj._over_object.stop_hover((0, 0), obj)
        if not self.acceptable_dnd_object(obj):
            return
        dom_event.preventDefault()
        if self.accept_drop(self.client_to_widget((dom_event.clientX, dom_event.clientY)), obj):
            dom_event.dataTransfer.dropEffect = 'move'
        else:
            dom_event.dataTransfer.dropEffect = 'none'

    def _drag_over(self, dom_event):
        obj = DnDItem.get_drag_item(dom_event)
        if obj is None:
            return
        if not obj._over_object == self:
            self._drag_enter(dom_event)
        else:
            if not self.acceptable_dnd_object(obj):
                return
            dom_event.preventDefault()
            if self.hover(self.client_to_widget((dom_event.clientX, dom_event.clientY)), obj):
                dom_event.dataTransfer.dropEffect = 'move'
            else:
                dom_event.dataTransfer.dropEffect = 'none'

    def _drag_enter(self, dom_event):
        obj = DnDItem.get_drag_item(dom_event)
        if obj is None:
            return
        if obj._over_object == self:
            return
        if obj._over_object is not None:
            obj._over_object.stop_hover((0, 0), obj)
        if not self.acceptable_dnd_object(obj):
            return
        dom_event.preventDefault()
        if self.start_hover(self.client_to_widget((dom_event.clientX, dom_event.clientY)), obj):
            dom_event.dataTransfer.dropEffect = 'move'
        else:
            dom_event.dataTransfer.dropEffect = 'none'
        obj._over_object = self

    def _drag_leave(self, dom_event):
        obj = DnDItem.get_drag_item(dom_event)
        if obj is None:
            return
        if not self.acceptable_dnd_object(obj):
            return
        dom_event.preventDefault()
        dom_event.dataTransfer.dropEffect = 'move'
        wx, wy = self.client_to_widget((dom_event.clientX, dom_event.clientY))
        if wx >= 0 and wx <= self.pixel_width and wy >= 0 and wy <= self.pixel_height:
            return
        self.stop_hover((wx, wy), obj)
        obj._over_object = None

    def start_hover(self, position, obj):
        """
            Called when an element is first dragged over a Target.
            If the target is willing to accept the element, this should
            return True. Otherwise it should return False.
        """
        return False

    def stop_hover(self, position, obj):
        """
            Called when an element is dragged out of a Target.
        """
        pass

    def hover(self, position, obj):
        """
            Called whenever an element is dragged over a Target. This
            is only called (barring browser implementation bugs) in between
            a start_hover and stop_hover event. Should return True if
            the Target is willing to accept a drop and False otherwise.
        """
        return False

    def accept_drop(self, position, obj):
        """
            Called whenever an element is dropped onto the Target. Should
            return True if the drop is accepted and False otherwise.
        """
        return False

    def acceptable_dnd_object(self, obj):
        return True


class Dialog(Widget):
    DEFAULT_CSS_CLASS = 'dialog'
    HTML_TEMPLATE = """
        <div id='header' class='dlg-header'>
            <div class='dlg-controls' id='controls'></div>
            <h2 class='dlg-title' id='title'></h2>
        </div>
        <div id='content' class='dlg-content'>
        </div>
        <div id='footer' class='dlg-footer'>
        </div>
    """

    def __init__(self, title, content_w, footer_w, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.content_w = content_w
        self.footer_w = footer_w
        self.find_child('title').innerHTML = title
        self.content_w.reparent(self.find_child('content'))
        self.footer_w.reparent(self.find_child('footer'))
        self.element.style.display = 'none'
        self.element.style.position = 'absolute'
        self.reparent(doc.body)

    def set_px_top(self, px_pos):
        self.element.style.top = str(px_pos)+'px'

    def open(self):
        self.element.style.display = 'initial'
        self.element.style.left = str((window.innerWidth-self.pixel_width)/2)+'px'
        start_top = -self.pixel_height
        final_top = (window.innerHeight-self.pixel_height)/2
        self.set_px_top(start_top)
        self.element.style.display = 'none'
        animate(start_top, final_top, self.set_px_top, 300, start_action=self.show)

    def close(self):
        start_top, y = self.screen_pos
        final_top = -self.pixel_height
        animate(start_top, final_top, self.set_px_top, 300, end_action=self.hide)


class OptionWidget(Widget):
    DEFAULT_TAG_NAME = 'option'

    def __init__(self, value, title=None, **kwargs):
        super().__init__(**kwargs)
        if title is None:
            title = value
        self.element.innerHTML=title
        self.element.value = title
