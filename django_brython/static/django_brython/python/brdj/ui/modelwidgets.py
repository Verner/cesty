from browser import html

import functools
import logging

import brdj.django.db.models as models
import asyncio


from .datawidgets import DataList
from .widgets import ButtonGroup, CancelButton, Dialog, OkButton, Widget

logger = logging.getLogger(__name__)


class FieldEditWidget(Widget):
    HTML_TEMPLATE = """
        <div class='form-group'>
            <label for='title' id='label'></label>
            <input class='form-control' type='text' id='input' onblur='_save_to_model'/>
        </div>
    """

    def __init__(self, model_instance, field_name, field, **kwargs):
        super().__init__(**kwargs)
        self._instance = model_instance
        self._field = field
        self._field_name = field_name
        self.find_child('label').innerHTML = self._field._title
        self._input = self.find_child('input')
        try:
            self.find_child('input').value = model_instance[field_name]
        except:
            self.find_child('input').value = self._field._default
        self._instance.bind('change', self._update_from_model)

    @property
    def value(self):
        return self._input.value

    @value.setter
    def value(self, val):
        self._input.value = val

    def _save_to_model(self, *args, **kwargs):
        self._instance[self._field_name] = self.value

    def _update_from_model(self, event):
        self.find_child('input').value = self._instance[self._field_name]


class CharFieldEditWidget(FieldEditWidget):
    pass


class TextFieldEditWidget(FieldEditWidget):
    pass


class ForeignKeyEditWidget(FieldEditWidget):
    pass


class ModelForm(Widget):
    DEFAULT_WIDGET_CLASSES = {
        models.CharField: CharFieldEditWidget,
        models.ForeignKey: ForeignKeyEditWidget,
        models.TextField: TextFieldEditWidget,
    }

    def _field_widget(self, fld, fld_name):
        w_type = self._wmap.get(type(fld), FieldEditWidget)
        return w_type(self._instance, fld_name, fld)

    def __init__(self, model_instance, widgets=None, **kwargs):
        super().__init__(**kwargs)
        self._instance = model_instance
        if widgets is None:
            self._wmap = self.DEFAULT_WIDGET_CLASSES
        else:
            self._wmap = widgets
        for (fld, fld_name) in self._instance._fields():
            if not fld._readonly:
                self <= self._field_widget(fld, fld_name)


class CreateInstanceDialog(Dialog):
    def __init__(self, model_class, widgets=None, **kwargs):
        self._instance = model_class()
        self._form = ModelForm(self._instance, widgets, **kwargs)
        self._controls = ButtonGroup([CancelButton(), OkButton('Create '+model_class.__name__)])
        super().__init__('Create '+model_class.__name__, self._form, self._controls)
        self._controls.bind('button_clicked', self.create)

    def create(self, ev):
        if ev['btn'].title.startswith('Create'):
            asyncio.ensure_future(self._instance.save())
        self.close()


class FieldViewWidget(Widget):
    DEFAULT_TAG_NAME = 'td'

    def _set_val(self, val):
        self.element.innerHTML = val

    def __init__(self, model_instance, fld, fld_name, **kwargs):
        super().__init__(**kwargs)
        self._instance = model_instance
        self._fld = fld
        self._name = fld_name
        self._instance.bind('change', self._update)
        self._set_val(self._instance[self._name])

    def _update(self, ev):
        if 'key' in ev and not ev['key'] == self._name:
            return
        self._set_val(self._instance[self._name])


@asyncio.async_class
class ForeignKeyViewWidget(Widget):
    DEFAULT_TAG_NAME = 'td'

    @asyncio.async_init
    def __init__(self, model_instance, fld, fld_name, **kwargs):
        super().__init__(**kwargs)
        if not isinstance(fld, models.ForeignKey):
            raise Exception("ForeignKeyViewWidget: field "+str(fld_name)+" is not a ForiegnKey")
        yield fld._foreign_model.objects.wait_for_load()
        try:
            self._instance = fld._foreign_model.objects.get(id=model_instance[fld_name])
            self._instance.bind('change', self._update)
            self.__set_val(str(self._instance))
        except Exception as ex:
            logger.error("Foreign object %s(id=%s) not found: %s", str(fld._foreign_model), model_instance[fld_name], str(ex))

    def __set_val(self, val):
        self.element.innerHTML = val

    def _update(self, ev):
        self.__set_val(str(self._instance))


class HiddenFieldView(Widget):
    DEFAULT_TAG_NAME = 'td'

    def __init__(self, model_instance, fld, fld_name, **kwargs):
        super().__init__(**kwargs)
        self._instance = model_instance
        self._fld = fld
        self._name = fld_name


class URLFieldViewWidget(FieldViewWidget):
    HTML_TEMPLATE = """
        <a id='link'></a>
    """
    _link = None

    def _set_val(self, val):
        if self._link is not None:
            self._link.href = val

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._link = self.find_child('link')
        self._link.innerHTML = self._name
        self._set_val(self._instance[self._name])


class ModelItemView(Widget):
    DEFAULT_TAG_NAME = 'tr'
    DEFAULT_WIDGET_CLASSES = {
        models.CharField: FieldViewWidget,
        models.ForeignKey: ForeignKeyViewWidget,
        models.TextField: FieldViewWidget,
        models.URLField: URLFieldViewWidget,
        models.OpaqueField: HiddenFieldView,
    }

    def __init__(self, model_instance, columns=None, widgets=None, **kwargs):
        super().__init__(**kwargs)
        if widgets is None:
            self._wmap = self.DEFAULT_WIDGET_CLASSES
        else:
            self._wmap = widgets
        self._instance = model_instance
        if columns is None:
            columns = [c for c in self._instance._fields() if not isinstance(c[1], models.OpaqueField)]
        else:
            columns = [(col_name, col_field) for (col_name, col_field) in self._instance._fields() if col_name in columns]
        for c_name, c_field in columns:
            w_class = self._wmap.get(type(c_field), None)
            if w_class is None:
                raise Exception("No widget for field of type: "+str(type(c_field)))
            self <= w_class(self._instance, c_field, c_name)


class ModelListView(DataList):
    class Meta:
        Model = None

    DEFAULT_TAG_NAME = 'table'
    HTML_TEMPLATE = """
        <thead>
            <tr id='head'></tr>
        </thead>
        <tbody main_element>
        </tbody>
    """

    def __init__(self, columns=None, widgets=None, **kwargs):
        super().__init__(item_widget_cls=functools.partial(ModelItemView, columns=columns, widgets=widgets),
                         data_source=self.Meta.model.objects.all(),
                         **kwargs)
        if columns is None:
            columns = [c for c in self.Meta.model._fields() if not isinstance(c[1], models.OpaqueField)]
        else:
            columns = [(col_name, col_type) for (col_name, col_type) in self._instance._fields() if col_name in columns]

        for cn, ct in columns:
            e = html.TH()
            if ct._title == '':
                e.innerHTML = cn.capitalize()
            else:
                e.innerHTML = ct._title
            self.find_child('head') <= e
