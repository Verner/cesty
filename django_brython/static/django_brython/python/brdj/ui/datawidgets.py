import copy
import functools
import logging

from brdj.events import EventSource
from brdj.observable import ObservableDict, ObservableList

from .widgets import Widget, OptionWidget

logger = logging.getLogger(__name__)


class DataList(Widget):
    DEFAULT_CSS_CLASS = 'list'

    def __init__(self, item_widget_cls, data_source=None,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        if data_source is None:
            self._src = ObservableList()
        else:
            self._src = data_source
        self._src.bind('insert', self._insert_handler)
        self._src.bind('delete', self._delete_handler)
        self._src.bind('clear', self._clear_handler)
        self._src.bind('extend', self._extend_handler)
        self._src.bind('sort', self._redraw)
        self._item_widget_cls = item_widget_cls
        self._redraw()

    @property
    def source(self):
        return self._src

    def _redraw(self, *args, **kwargs):
        self.clear_children()
        for item in self._src:
            self.append_child(self._item_widget_cls(item))

    def _extend_handler(self, event):
        for item in self._src._data[event['first']:]:
            self.append_child(self._item_widget_cls(item))

    def _clear_handler(self, event):
        self.clear_children()

    def _delete_handler(self, event):
        index = event['index']
        w = self.children[index]
        self.remove_child(w)

    def _insert_handler(self, event):
        index, obj = event['index'], event['obj']
        w = self._item_widget_cls(obj)
        if index >= len(self.children):
            self.append_child(w)
        else:
            before = self.children[index]
            self.insert_child(w, before)


class DataOptionWidget(OptionWidget):

    @classmethod
    def factory(cls, value_key, title_key):
        def w(data_item, **kwargs):
            return DataOptionWidget(value_key=value_key, title_key=title_key, data_item=data_item, **kwargs)
        return w

    def __init__(self, value_key, title_key, data_item, **kwargs):
        super().__init__(value=data_item[value_key], title=data_item[title_key], **kwargs)
        self._v_key = value_key
        self._t_key = title_key
        self._data_item = data_item
        self._data_item.bind('change', self._update)
        self._update()

    def _update(self, event=None):
        try:
            self.element.innerHTML = self._data_item[self._v_key]
            self.element.value = self._data_item[self._v_key]
        except:
            pass


class SelectInput(DataList):
    DEFAULT_CSS_CLASS = 'input-select'
    DEFAULT_TAG_NAME = 'select'

    def __init__(self, data_source, **kwargs):
        super().__init__(item_widget_cls=DataOptionWidget.factory, data_source=data_source, **kwargs)

